import { CurrentUserService } from './../../../core/services/current-user.service';
import { loginDTO } from './../../../shared/models/DTO/loginDTO';
import { StorageHelper } from './../../../core/helpers/storage';
import { AuthService } from './../../../core/api/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Urls } from 'src/app/core/helpers/urls';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  passwordVisible: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private currentUserService: CurrentUserService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true],
    });
  }

  submitForm(): void {
    let payload: { email: string; password: string } | null = {
      email: this.loginForm.get('email')!.value,
      password: this.loginForm.get('password')!.value,
    };

    this.authService.logIn(payload).subscribe(
      (res: loginDTO) => {
        if (res.isSuccessful == false) {
          this.message.error(res.message, { nzDuration: 5000 });
        } else {
          StorageHelper.saveJWTInLocalStorage(res.token);
          StorageHelper.savecurrentUserIdInLocalStorage(res.user.id.toString())
          this.currentUserService.setUser(res.user);

          this.router.navigate(['/admin/dashboard']);
        }
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  redirectToHomepage(){
    this.router.navigateByUrl(Urls.HOMEPAGE);
  }
}
