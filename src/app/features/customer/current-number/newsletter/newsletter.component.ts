import { NewsletterEditionsService } from './../../../../core/api/newsletter-editions.service';
import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Newsletter } from 'src/app/shared/models/ViewModels/newsletter';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss'],
})
export class NewsletterComponent implements OnInit {
  newsletter!: Newsletter;

  error:boolean=false;
  loading:boolean=false;
  showOption:string='card';
  innerWidth: any;

  constructor(
    private newsletterService: NewsletterEditionsService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;

    this.loading=true;
    this.getCurrentNewsletter();
  }

  getCurrentNewsletter() {
    this.newsletterService.getCurrentEdition().subscribe(
      (res) => {
        this.newsletter = res[0];
      },
      (error) => {
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      },()=>{
        this.loading=false;
      }
    );
  }

  onError(error: any) {
    this.error=true;
  }
}
