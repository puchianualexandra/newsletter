import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CollaboratorsLabelsService } from 'src/app/core/api/collaborators-labels.service';
import { CollaboratorsService } from 'src/app/core/api/collaborators.service';
import { FacultyService } from 'src/app/core/api/faculty.service';
import { Collaborator } from 'src/app/shared/models/ViewModels/Collaborator';
import { Simple } from 'src/app/shared/models/ViewModels/Simple';

@Component({
  selector: 'app-contributors-page',
  templateUrl: './contributors-page.component.html',
  styleUrls: ['./contributors-page.component.scss'],
})
export class ContributorsPageComponent implements OnInit {
  contributorsList: Array<Collaborator> = new Array<Collaborator>();
  totalNo: number = 0;

  collaboratorLabels: Array<Simple> = new Array<Simple>();
  facultiesList: Array<Simple> = new Array<Simple>();

  innerWidth: any;
  loader: boolean = false;
  filterData: {
    pageSize: number;
    pageNo: number;
    name: string;
  } = {
    pageSize: 5,
    pageNo: 1,
    name: '',
  };

  constructor(
    private collaboratorService: CollaboratorsService,
    private message: NzMessageService,
    private collaboratorsLabelsService: CollaboratorsLabelsService,
    private facultyService: FacultyService,
  ) {}

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.loader = true;
    
    this.getVolunteersList();
  }

  getVolunteersList() {
    this.collaboratorService
      .getCollaboratorsWithFilters(this.filterData)
      .subscribe(
        (res) => {
          this.contributorsList = res.collaborators;
          this.totalNo = res.totalNo;
          this.getCollaboratorLabels()
          this.getFacultiesList();
        },
        (error) => {
          console.error(error);
          this.message.error('Error! Please try again later.', {
            nzDuration: 5000,
          });
        },()=>{
          this.loader = false;
        }
      );
  }

  getCollaboratorLabels() {
    this.collaboratorsLabelsService.getLabels().subscribe((res) => {
      this.collaboratorLabels = res;
    });
  }

  getFacultiesList() {
    this.facultyService.getFaculties().subscribe((res) => {
      this.facultiesList = res;
    });
  }

  cleanSearchInput(){
    this.filterData.name='';
    this.loader=true;
    this.getVolunteersList();
  }
}
