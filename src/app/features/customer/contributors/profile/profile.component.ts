import { Component, Input, OnInit } from '@angular/core';
import { Collaborator } from 'src/app/shared/models/ViewModels/Collaborator';
import { Simple } from 'src/app/shared/models/ViewModels/Simple';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @Input() collaborator!: Collaborator;
  @Input() collaboratorLabels: Array<Simple> = new Array<Simple>();
  @Input() facultiesList: Array<Simple> = new Array<Simple>();

  openedCollaborator!:Collaborator;
  isModalVisible:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  showModal(collaborator:Collaborator): void {
    this.openedCollaborator=collaborator;
    this.isModalVisible = true;
  }

  handleCloseArticlesModal() {
    this.isModalVisible = false;
  }

  getLabelName(id:number):string{
    const result = this.collaboratorLabels?.filter(obj => obj.id == id);
    return result[0]?.name;
  }

  getFacultyName(id:number):string{
    const result = this.facultiesList?.filter(obj => obj.id == id);
    return result[0]?.name;
   }

}
