import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NewsletterEditionsService } from 'src/app/core/api/newsletter-editions.service';
import { Newsletter } from 'src/app/shared/models/ViewModels/newsletter';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss'],
})
export class ArchiveComponent implements OnInit {
  newsletterArchive: Array<Newsletter> = new Array<Newsletter>();
  yearsList: Array<any> = [];
  selectedYear: number = 2020;

  loader:boolean=false;
  innerWidth: any;

  constructor(
    private newsletterEditionsService: NewsletterEditionsService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.loader=true;
    
    this.getYearsList();
    this.getListOfNewsletters();
  }

  getListOfNewsletters() {
    this.newsletterEditionsService
      .getNewslettersByYear(this.selectedYear)
      .subscribe(
        (res) => {
          this.newsletterArchive = res;
          this.loader=false;
        },
        (error) => {
          console.error(error);
          this.message.error('Error! Please try again later.', {
            nzDuration: 5000,
          });
        }
      );
  }

  getYearsList() {
    let currentYear = new Date().getFullYear();
    for (let year = currentYear; year >= 2018; year--) {
      this.yearsList.push(year);
    }
    this.selectedYear = this.yearsList[0];
  }
}
