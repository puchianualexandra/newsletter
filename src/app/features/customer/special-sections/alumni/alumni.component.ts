import { CollaboratorsService } from './../../../../core/api/collaborators.service';
import { Collaborator } from './../../../../shared/models/ViewModels/Collaborator';
import { ArticleItem } from './../../../../shared/models/ViewModels/articleItem';
import { ArticleService } from './../../../../core/api/article.service';
import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/shared/models/ViewModels/article';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-alumni',
  templateUrl: './alumni.component.html',
  styleUrls: ['./alumni.component.scss'],
})
export class AlumniComponent implements OnInit {
  listOfArticles: Array<Article> = new Array<Article>();
  totalNo: number = 0;
  openedArticle!: Article;

  isPreviewModalOpen: boolean = false;
  loader: boolean = false;
  innerWidth: any;

  filterData: {
    pageSize: number;
    pageNo: number;
    title: string;
    specialSection: string;
  } = {
    pageSize: 5,
    pageNo: 1,
    title: '',
    specialSection: 'alumni',
  };

  constructor(
    private articleService: ArticleService,
    private collaboratorService: CollaboratorsService,
    private message: NzMessageService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.route.queryParams.subscribe((params) => {
      this.filterData.title = params['titleArticle'];
    });

    this.getArticles();
  }

  getArticles() {
    this.loader = true;
    this.articleService
      .getArticlesWithFiltersOnUserSide(this.filterData)
      .subscribe(
        (res) => {
          this.listOfArticles = res.articles;
          this.totalNo = res.totalNo;
          if(this.totalNo>0){
            this.getCollaborators();
          }else{
            this.loader=false;
          }
        },
        (error) => {
          console.error(error);
          this.message.error('Error! Please try again later.', {
            nzDuration: 5000,
          });
        }
      );
  }

  getArticleItems(article: Article) {
    this.openedArticle = article;
    this.articleService.getArticleItems(article.id).subscribe(
      (res: ArticleItem[]) => {
        this.openedArticle.articleItemsDTO = res;
        this.isPreviewModalOpen = true;
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  getCollaborators() {
    this.listOfArticles.forEach((article) => {
      this.collaboratorService
        .getCollaboratorById(article.collaboratorId)
        .subscribe((res) => {
          article.collaborator = res;
          this.loader = false;
        });
    });
  }

  cleanSearchInput(){
    this.filterData.title='';
    this.loader=true;
    this.getArticles();
  }

  handleCloseViewModal() {
    this.openedArticle = {
      id: 0,
      title: '',
      dateOfUpload: new Date(),
      collaboratorId: 0,
      collaborator: {} as Collaborator,
      description: '',
      isPosted: false,
      specialSection: '',
      articleItemsDTO: [],
    };
    this.isPreviewModalOpen = false;
  }
}
