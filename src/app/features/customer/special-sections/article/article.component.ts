import { FacultyService } from './../../../../core/api/faculty.service';
import { Simple } from './../../../../shared/models/ViewModels/Simple';
import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
} from '@angular/core';
import { Article } from 'src/app/shared/models/ViewModels/article';
import { CollaboratorsLabelsService } from 'src/app/core/api/collaborators-labels.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent implements OnInit {
  @Input() article!: Article;
  @Output() openViewMode: EventEmitter<any> = new EventEmitter<any>();

  labels!: Simple[];
  faculties!: Simple[];

  constructor(
    private labelsService: CollaboratorsLabelsService,
    private facultyService: FacultyService
  ) {}

  ngOnInit(): void {
    this.getLabels();
    this.getFaculties();
  }

  getLabels() {
    this.labelsService.getLabels().subscribe((res) => {
      this.labels = res;
    });
  }

  getFaculties() {
    this.facultyService.getFaculties().subscribe((res) => {
      this.faculties = res;
    });
  }

  getLabelNameById(id: number) {
    if (this.labels) {
      const result = this.labels?.filter((obj) => obj.id == id);
      return result[0]?.name;
    }
    return;
  }

  getFacultyNameById(id: number | undefined) {
    if (this.faculties && id) {
      const result = this.faculties?.filter((obj) => obj.id == id);
      return result[0]?.name;
    }
    return;
  }
}
