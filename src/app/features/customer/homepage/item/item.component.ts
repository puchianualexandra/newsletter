import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {
  @Input() item!: { count: number; icon: string; textEn: string, textRo:string };

  currentLanguage:string='ro';

  constructor(private translate: TranslateService) {
    this.translate.onLangChange.subscribe((event: any) => {
      this.currentLanguage=this.translate.currentLang
    });
  }

  ngOnInit(): void {}
}
