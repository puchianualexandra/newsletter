import { CollaboratorsService } from 'src/app/core/api/collaborators.service';
import { NewsletterEditionsService } from 'src/app/core/api/newsletter-editions.service';
import { Component, OnInit } from '@angular/core';
import { ArticleService } from 'src/app/core/api/article.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
})
export class HomepageComponent implements OnInit {
  itemsList: Array<{ count: number; icon: string; textEn: string, textRo:string }> = [];

  constructor(
    private newsletterService: NewsletterEditionsService,
    private collaboratorService: CollaboratorsService,
    private articlesService: ArticleService
  ) {}

  ngOnInit(): void {
    this.getNewsletters();
    this.getArticles();
    this.getCollaborators();
  }

  getNewsletters() {
    this.newsletterService.getCount().subscribe((res) => {
      this.itemsList.push({
        count: res,
        icon: 'read',
        textEn: 'Newsletters',
        textRo:'Newslettere'
      });
    });
  }

  getArticles() {
    this.articlesService.getCount().subscribe((res) => {
      this.itemsList.push({
        count: res,
        icon: 'file-text',
        textEn: 'Articles',
        textRo:'Articole'
      });
    });
  }

  getCollaborators() {
    this.collaboratorService.getCount().subscribe((res) => {
      this.itemsList.push({
        count: res,
        icon: 'user',
        textEn: 'Collaborators',
        textRo:'Colaboratori'
      });
    });
  }
}
