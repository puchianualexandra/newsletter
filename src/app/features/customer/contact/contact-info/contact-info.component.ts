import { SubscriberService } from './../../../../core/api/subscriber.service';
import { Component } from '@angular/core';
import { Subscriber } from 'src/app/shared/models/ViewModels/subscriber';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CustomValidators } from 'src/app/shared/utils/custom-validators';

@Component({
  selector: 'app-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.scss'],
})
export class ContactInfoComponent {
  unsubscribeEmail: string = '';
  subscriber!: Subscriber;

  constructor(
    private subscriberService: SubscriberService,
    private message: NzMessageService
  ) {}

  unsubscribe() {
    if (this.unsubscribeEmail != '') {
      if (CustomValidators.validateEmail(this.unsubscribeEmail) == false) {
        this.message.warning('You have to enter a valid email!', {
          nzDuration: 5000,
        });
      } else {
        this.getSubscriberByEmail();
      }
    }
  }

  getSubscriberByEmail() {
    this.subscriberService
      .getSubscriberByEmail(this.unsubscribeEmail)
      .subscribe(
        (res) => {
          if (res.length == 0) {
            this.message.error("Error! The subscriber doesn't exist.", {
              nzDuration: 5000,
            });
          } else {
            this.subscriber = res[0];
            this.deleteSubscriber();
          }
        },
        (error) => {
          console.error(error);
        }
      );
  }

  deleteSubscriber() {
    this.subscriberService.deleteSubscriber(this.subscriber.id).subscribe(
      (res) => {
        this.message.success('Succes! You have unsubscribed successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }
}
