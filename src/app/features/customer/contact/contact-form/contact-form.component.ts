import { Suggestion } from 'src/app/shared/models/ViewModels/Suggestion';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SuggestionService } from 'src/app/core/api/suggestion.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
})
export class ContactFormComponent implements OnInit {
  contactForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private suggestionService: SuggestionService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.initiliseForm();
  }

  initiliseForm() {
    this.contactForm = this.fb.group({
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      email: [null, [Validators.required]],
      subject: [null, [Validators.required]],
      description: [null, [Validators.required]],
    });
  }

  sendContactForm() {
    let payloadData: Suggestion = {
      id: 0,
      firstName: this.contactForm.get('firstName')?.value,
      lastName: this.contactForm.get('lastName')?.value,
      email: this.contactForm.get('email')?.value,
      subject: this.contactForm.get('subject')?.value,
      description: this.contactForm.get('description')?.value,
      dateOfMessage: new Date(),
      isNew:true
    };

    this.suggestionService.postSuggestion(payloadData).subscribe(
      (res) => {
        this.contactForm.reset()
        this.initiliseForm();
        this.message.success('The form was send successfully!', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }
}
