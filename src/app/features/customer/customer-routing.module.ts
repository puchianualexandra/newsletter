import { OthersComponent } from './special-sections/others/others.component';
import { TeachersComponent } from './special-sections/teachers/teachers.component';
import { GraduatesComponent } from './special-sections/graduates/graduates.component';
import { AlumniComponent } from './special-sections/alumni/alumni.component';
import { ArchiveComponent } from './archive/archive-page/archive.component';
import { HomepageComponent } from './homepage/homepage/homepage.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContributorsPageComponent } from './contributors/contributors-page/contributors-page.component';
import { ContactPageComponent } from './contact/contact-page/contact-page.component';
import { NewsletterComponent } from './current-number/newsletter/newsletter.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'homepage',
    pathMatch: 'full'
  },
  {
    path: 'homepage',
    component: HomepageComponent,
  },
  {
    path: 'current-number',
    component: NewsletterComponent,
  },
  {
    path: 'archive',
    component: ArchiveComponent,
  },
  {
    path: 'special-sections',
    children: [
      {
        path: '',
        redirectTo: 'alumni',
      },
      {
        path: 'alumni',
        component: AlumniComponent,
      },
      {
        path: 'graduates',
        component: GraduatesComponent,
      },
      {
        path: 'teachers',
        component: TeachersComponent,
      },
      {
        path: 'others',
        component: OthersComponent,
      },
    ],
  },
  {
    path: 'contributors',
    component: ContributorsPageComponent,
  },
  {
    path: 'contact',
    component: ContactPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerRoutingModule {}
