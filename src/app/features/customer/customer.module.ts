import { TeachersComponent } from './special-sections/teachers/teachers.component';
import { GraduatesComponent } from './special-sections/graduates/graduates.component';
import { AlumniComponent } from './special-sections/alumni/alumni.component';
import { ItemComponent } from './homepage/item/item.component';
import { HomepageComponent } from './homepage/homepage/homepage.component';
import { NewsletterComponent } from './current-number/newsletter/newsletter.component';
import { ProfileComponent } from './contributors/profile/profile.component';
import { ContactInfoComponent } from './contact/contact-info/contact-info.component';
import { ContactFormComponent } from './contact/contact-form/contact-form.component';
import { ArchiveComponent } from './archive/archive-page/archive.component';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { ContactPageComponent } from './contact/contact-page/contact-page.component';
import { ContributorsPageComponent } from './contributors/contributors-page/contributors-page.component';
import { ArticleComponent } from './special-sections/article/article.component';
import { OthersComponent } from './special-sections/others/others.component';

import { PdfViewerModule } from 'ng2-pdf-viewer';


@NgModule({
  declarations: [
    ArchiveComponent,
    ContactPageComponent,
    ContactFormComponent,
    ContactInfoComponent,
    ContributorsPageComponent,
    ProfileComponent,
    NewsletterComponent,
    HomepageComponent,
    ItemComponent,
    AlumniComponent,
    GraduatesComponent,
    TeachersComponent,
    ArticleComponent,
    OthersComponent,
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    SharedModule,

    //pdf viewer
    PdfViewerModule,
  ],
})
export class CustomerModule {}
