import { SuggestionsPageComponent } from './tabs/suggestions-page/suggestions-page.component';
import { NewslettersPageComponent } from './tabs/newsletters-edition-page/newsletters-page/newsletters-page.component';
import { SubscribersPageComponent } from './tabs/subscribers-page/subscribers-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { CollaboratorsPageComponent } from './tabs/collaborators-page/collaborators-page/collaborators-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
 { path: 'dashboard',
    component: AdminDashboardComponent,
    children: [
      {
        path: '',
        redirectTo:'users-page',
      },
      {
        path: 'users-page',
        component: SubscribersPageComponent,
      },
      {
        path: 'subscribers-page',
        component: SubscribersPageComponent,
      },
      {
        path: 'collaborators-page',
        component: CollaboratorsPageComponent,
      },
      {
        path: 'newsletters-page',
        component: NewslettersPageComponent,
      },
      {
        path: 'articles-page',
        component: SubscribersPageComponent,
      },
      {
        path: 'suggestions-page',
        component: SuggestionsPageComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
