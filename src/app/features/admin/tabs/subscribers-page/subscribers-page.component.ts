import { SubscriberService } from '../../../../core/api/subscriber.service';
import { Component, OnInit } from '@angular/core';
import { Subscriber } from 'src/app/shared/models/ViewModels/subscriber';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-subscribers-page',
  templateUrl: './subscribers-page.component.html',
  styleUrls: ['./subscribers-page.component.scss'],
})
export class SubscribersPageComponent implements OnInit {
  listOfSubscribers!: Subscriber[];
  totalNo: number = 0;
  loader: boolean = false;

  isSearchVisible: boolean = false;
  filterData: {
    pageSize: number;
    pageNo: number;
    email: string;
  } = {
    pageSize: 10,
    pageNo: 1,
    email: '',
  };

  constructor(
    private subscriberService: SubscriberService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.getListOfSubscribers();
  }

  resetSearch(): void {
    this.filterData.email = '';
    this.searchByEmail();
  }

  searchByEmail(): void {
    this.isSearchVisible = false;
    this.getListOfSubscribers();
  }

  getListOfSubscribers() {
    this.loader = true;
    this.subscriberService.getSubscribersWithFilters(this.filterData).subscribe(
      (res) => {
        this.listOfSubscribers = res.subscribers;
        this.totalNo = res.totalNo;
        this.loader = false;
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  deleteSubscriberById(id: number) {
    this.loader=true;
    this.subscriberService.deleteSubscriber(id).subscribe(
      (res) => {
        this.getListOfSubscribers();
        this.message.success('The subscriber was deleted successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error(
          'There was a problem deleting the subscriber. Please try again later.',
          {
            nzDuration: 5000,
          }
        );
      }
    );
  }
}
