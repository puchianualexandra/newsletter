import { SuggestionService } from './../../../../core/api/suggestion.service';
import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Suggestion } from 'src/app/shared/models/ViewModels/Suggestion';

@Component({
  selector: 'app-suggestions-page',
  templateUrl: './suggestions-page.component.html',
  styleUrls: ['./suggestions-page.component.scss']
})
export class SuggestionsPageComponent implements OnInit {
  listOfSuggestions!: Suggestion[];
  totalNo: number = 0;
  loader: boolean = false;

  isSearchVisible: boolean = false;
  filterData: {
    pageSize: number;
    pageNo: number;
    name: string;
  } = {
    pageSize: 10,
    pageNo: 1,
    name: '',
  };

  constructor(
    private suggestionService: SuggestionService,
    private message: NzMessageService
  ) { }

  ngOnInit(): void {
    this.getListOfSuggestions();
  }

  getListOfSuggestions(){
    this.loader=true;
    this.suggestionService.getSuggestionsWithFilters(this.filterData).subscribe(
      (res)=>{
        this.listOfSuggestions = res.suggestions;
        this.totalNo = res.totalNo;
        this.loader = false;
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    )
  }

  deleteSuggestionById(id: number) {
    this.loader=true;
    this.suggestionService.deleteSuggestion(id).subscribe(
      (res) => {
        this.getListOfSuggestions();
        this.message.success('The suggestion was deleted successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error(
          'There was a problem deleting the subscriber. Please try again later.',
          {
            nzDuration: 5000,
          }
        );
      }
    );
  }

  resetSearch(): void {
    this.filterData.name = '';
    this.searchByName();
  }

  searchByName(): void {
    this.isSearchVisible = false;
    this.loader = true;
    this.getListOfSuggestions();
  }

}
