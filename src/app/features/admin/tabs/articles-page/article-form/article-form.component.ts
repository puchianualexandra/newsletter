import { CollaboratorsService } from './../../../../../core/api/collaborators.service';
import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { Article } from 'src/app/shared/models/ViewModels/article';
import { Collaborator } from 'src/app/shared/models/ViewModels/Collaborator';
import { Simple } from 'src/app/shared/models/ViewModels/Simple';
import { differenceInCalendarDays } from 'date-fns';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.scss'],
})
export class ArticleFormComponent implements OnInit, OnChanges {
  @Input() article!: Article;
  @Input() labelsList: Array<Simple> = new Array<Simple>();
  @Input() facultiesList: Array<Simple> = new Array<Simple>();

  collaboratorsList: Array<Collaborator> = new Array<Collaborator>();
  numberOfElements: number = 0;

  constructor(private collaboratorService: CollaboratorsService) {}

  ngOnInit(): void {
    this.getCollaboratorsList();
  }

  ngOnChanges() {
    if (this.article?.id==0) {
      this.article.collaboratorId = null;
    }
  }

  getCollaboratorsList() {
    this.collaboratorService.getCollaborators().subscribe((res) => {
      this.collaboratorsList = res;
    });
  }

  getCollaboratorDetails(id: number | null) {
    this.collaboratorService.getCollaboratorById(id).subscribe((res) => {
      this.article.collaborator = res;
    });
  }

  deleteItem(index: any) {
    this.article.articleItemsDTO.splice(index, 1);
  }

  disabledDate = (current: Date): boolean => {
    return differenceInCalendarDays(current, new Date()) > 0;
  };

  addImage() {
    this.numberOfElements++;
    this.article.articleItemsDTO.push({
      id: 0,
      type: 'photo',
      contentImage: '',
      contentText: '',
    });
  }

  addText() {
    this.numberOfElements++;
    this.article.articleItemsDTO.push({
      id: 0,
      type: 'text',
      contentImage: '',
      contentText: '',
    });
  }

  addTextImageLeft() {
    this.numberOfElements++;
    this.article.articleItemsDTO.push({
      id: 0,
      type: 'photo-text',
      contentImage: '',
      contentText: '',
    });
  }

  addTextImageRight() {
    this.numberOfElements++;
    this.article.articleItemsDTO.push({
      id: 0,
      type: 'text-photo',
      contentImage: '',
      contentText: '',
    });
  }
}
