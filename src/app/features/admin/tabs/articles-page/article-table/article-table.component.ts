import { ArticleService } from 'src/app/core/api/article.service';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Article } from 'src/app/shared/models/ViewModels/article';
import { CollaboratorsService } from 'src/app/core/api/collaborators.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Simple } from 'src/app/shared/models/ViewModels/Simple';

@Component({
  selector: 'app-article-table',
  templateUrl: './article-table.component.html',
  styleUrls: ['./article-table.component.scss'],
})
export class ArticleTableComponent implements OnInit {
  @Input() labelsList:Array<Simple>=new Array<Simple>();
  @Input() facultiesList:Array<Simple>=new Array<Simple>(); 
  @Input() reloadTable!: number;

  @Output() openEditModal: EventEmitter<Article> = new EventEmitter<Article>();
  @Output() openPreviewArticle: EventEmitter<Article> = new EventEmitter<Article>();

  listOfArticles: Array<Article> = new Array<Article>();
  totalNo!: number;

  loader: boolean = false;
  isTitleSearchVisible: boolean = false;

  filterData: {
    pageSize: number;
    pageNo: number;
    title: string;
  } = {
    pageSize: 5,
    pageNo: 1,
    title: '',
  };

  constructor(
    private articleService: ArticleService,
    private collaboratorService: CollaboratorsService,
    private message: NzMessageService
) {}

  ngOnInit(): void {
    this.getListOfArticoles();
  }

  ngOnChanges() {
    this.getListOfArticoles();
  }

  getListOfArticoles() {
    this.loader = true;
    this.articleService
      .getArticlesWithFiltersOnAdminSide(this.filterData)
      .subscribe(
        (res) => {
          this.listOfArticles = res.articles;
          this.totalNo = res.totalNo;
          this.getCollaborators();
        },
        (error) => {
          console.error(error);
          this.message.error('Error! Please try again later.', {
            nzDuration: 5000,
          });
        }
      );
  }

  getCollaborators() {
    this.listOfArticles.forEach((article) => {
      this.collaboratorService
        .getCollaboratorById(article.collaboratorId)
        .subscribe((res) => {
          article.collaborator = res;
        },(error)=>{
          console.error(error)
        },()=>{
          this.loader = false;
        });
    });
  }

  editArticle(article:Article) {
    this.openEditModal.emit(article)
  }

  deleteArticleById(id: number) {
    this.articleService.deleteArticle(id).subscribe(
      (res)=>{
        this.getListOfArticoles();
        this.message.success('The article was deleted successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    )
  }

  changeIsPosted(id:number) {
    this.articleService.changeIsPosted(id).subscribe(
      (res)=>{
        this.getListOfArticoles();
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    )
  }

  // Search by title
  resetSearch(): void {
    this.filterData.title = '';
    this.searchByTitle();
  }

  searchByTitle(): void {
    this.isTitleSearchVisible = false;
    this.getListOfArticoles();
  }

  // Preview article modal
  previewArticle(article:Article) {
    this.openPreviewArticle.emit(article)
  }

  getLabelNameById(id:number){
    const result = this.labelsList?.filter((obj) => obj.id == id);
    return result[0]?.name;
  }

  getFacultyByName(id:number | undefined){
    if(id){
      const result = this.facultiesList?.filter((obj) => obj.id == id);
      return result[0]?.name;
    }
   return
  }
}
