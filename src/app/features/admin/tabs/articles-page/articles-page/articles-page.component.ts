import { ArticleService } from 'src/app/core/api/article.service';
import { ArticleItem } from './../../../../../shared/models/ViewModels/articleItem';
import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/shared/models/ViewModels/article';
import { Simple } from 'src/app/shared/models/ViewModels/Simple';
import { CollaboratorsLabelsService } from 'src/app/core/api/collaborators-labels.service';
import { Collaborator } from 'src/app/shared/models/ViewModels/Collaborator';
import { NzMessageService } from 'ng-zorro-antd/message';
import { FacultyService } from 'src/app/core/api/faculty.service';

@Component({
  selector: 'app-articles-page',
  templateUrl: './articles-page.component.html',
  styleUrls: ['./articles-page.component.scss'],
})
export class ArticlesPageComponent implements OnInit {
  article!: Article;

  labelsList!: Array<Simple>;
  facultiesList!: Array<Simple>;

  reloadTable: number = 0;
  isModalVisible: boolean = false;
  isPreviewModalOpen:boolean=false;

  constructor(
    private articleService: ArticleService,
    private labelsService: CollaboratorsLabelsService,
    private facultiesService: FacultyService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.getLabels();
    this.getFaculties();
  }

  getLabels() {
    this.labelsService.getLabels().subscribe((res) => {
      this.labelsList = res;
    });
  }

  getFaculties() {
    this.facultiesService.getFaculties().subscribe((res) => {
      this.facultiesList = res;
    });
  }

  getArticleItems() {
    this.articleService.getArticleItems(this.article.id).subscribe(
      (res: ArticleItem[]) => {
        this.article.articleItemsDTO = res;
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  saveArticle() {
    this.isModalVisible = false;
    delete this.article.collaborator;

    if (this.article.id == 0 || this.article.id == null) {
      this.addNewArticle();
    } else {
      this.updateArticle();
    }
  }

  addNewArticle() {
    this.articleService.postArticle(this.article).subscribe(
      (res) => {
        this.reloadTable++;
        this.isModalVisible = false;
        this.message.success('The article has been added successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  updateArticle() {
    this.articleService.updateArticle(this.article).subscribe(
      (res) => {
        this.reloadTable++;
        this.isModalVisible = false;
        this.message.success('The article has been updated successfully..', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  openEditArticleModal(article: Article) {
    this.article = article;
    this.isModalVisible = true;
    this.getArticleItems();
  }

  openPreviewArticleModal(article:Article){
    this.article = article;
    this.isPreviewModalOpen=true;
    this.getArticleItems();
  }

  getItemsForPreview(article: Article) {
    this.article = article;
    this.getArticleItems();
  }

  openAddNewArticleModal() {
    this.isModalVisible = true;
    this.article = {
      id: 0,
      title: '',
      dateOfUpload: new Date(),
      collaboratorId: 0,
      collaborator: {} as Collaborator,
      description: '',
      isPosted: false,
      specialSection: '',
      articleItemsDTO: [],
    };
  }

  isFormValid() {
    return !(
      this.article.title == '' ||
      this.article.collaboratorId == null ||
      this.article.specialSection == null ||
      this.article.dateOfUpload == null ||
      this.article.description == ''
    );
  }
}
