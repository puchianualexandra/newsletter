import { AdminService } from './../../../../../core/api/admin.service';
import { Admin } from 'src/app/shared/models/ViewModels/Admin';
import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CurrentUserService } from 'src/app/core/services/current-user.service';
import { CustomValidators } from 'src/app/shared/utils/custom-validators';

@Component({
  selector: 'app-admins-page',
  templateUrl: './admins-page.component.html',
  styleUrls: ['./admins-page.component.scss'],
})
export class AdminsPageComponent implements OnInit {
  isAddAdminModalVisible: boolean = false;
  admin!: Admin;
  reloadTable: number = 0;

  constructor(
    private message: NzMessageService,
    private adminService: AdminService,
    private currentUserService: CurrentUserService
  ) {}

  ngOnInit(): void {}

  openAddAdminModal() {
    this.isAddAdminModalVisible = true;
    this.admin = {
      id: 0,
      firstName: '',
      lastName: '',
      email: '',
      phoneNumber: '',
      password: '',
      dateOfJoin: new Date(),
      isNew: true,
      role: 'Admin',
    };
  }

  registerAdmin() {
    this.adminService.registerAdmin(this.admin).subscribe(
      (res) => {
        if (res.isSuccessful == false) {
          this.message.error(res.message, {
            nzDuration: 5000,
          });
        } else {
          this.reloadTable++;
          this.isAddAdminModalVisible = false;
          this.message.success('The admin was added successfully.', {
            nzDuration: 5000,
          });
        }
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  isFormValid() {
    return !(
      this.admin.firstName == '' ||
      this.admin.lastName == '' ||
      this.admin.password == '' ||
      this.admin.email == '' ||
      this.admin.phoneNumber == ''||
      !CustomValidators.validateEmail(this.admin.email)
    );
  }

  handleCancel() {
    this.isAddAdminModalVisible = false;
  }

  isSuperAdmin(): boolean {
    return this.currentUserService.isSuperAdmin();
  }
}
