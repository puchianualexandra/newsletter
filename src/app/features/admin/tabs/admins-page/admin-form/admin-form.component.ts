import { Component, Input, OnInit } from '@angular/core';
import { Admin } from 'src/app/shared/models/ViewModels/Admin';

@Component({
  selector: 'app-admin-form',
  templateUrl: './admin-form.component.html',
  styleUrls: ['./admin-form.component.scss']
})
export class AdminFormComponent implements OnInit {
  @Input() admin!:Admin;
  passwordVisible: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
