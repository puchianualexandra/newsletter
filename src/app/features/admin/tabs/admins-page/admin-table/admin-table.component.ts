import { CurrentUserService } from './../../../../../core/services/current-user.service';
import { AdminService } from './../../../../../core/api/admin.service';
import { Admin } from 'src/app/shared/models/ViewModels/Admin';
import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-admin-table',
  templateUrl: './admin-table.component.html',
  styleUrls: ['./admin-table.component.scss'],
})
export class AdminTableComponent implements OnInit {
  @Input() reloadTable!: number;

  isSearchVisible: boolean = false;
  listOfAdmins: Array<Admin> = new Array<Admin>();
  loader: boolean = false;
  totalNo!: number;
  filterData: {
    pageSize: number;
    pageNo: number;
    name: string;
  } = {
    pageSize: 10,
    pageNo: 1,
    name: '',
  };

  constructor(
    private adminService: AdminService,
    private message: NzMessageService,
    private currentUserService: CurrentUserService
  ) {}

  ngOnInit(): void {
    this.getAdmins();
  }

  ngOnChanges() {
    this.getAdmins();
  }

  getAdmins() {
    this.loader = true;
    this.adminService.getAdminsWithFilters(this.filterData).subscribe(
      (res) => {
        this.listOfAdmins = res.admins;
        this.totalNo = res.totalNo;
        this.loader = false;
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  deleteAdminById(id: number) {
    this.loader = true;
    this.adminService.deleteAdmin(id).subscribe(
      (res) => {
        this.getAdmins();
        this.message.success('The suggestion was deleted successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error(
          'There was a problem deleting the subscriber. Please try again later.',
          {
            nzDuration: 5000,
          }
        );
      }
    );
  }

  resetSearch(): void {
    this.filterData.name = '';
    this.searchByName();
  }

  searchByName(): void {
    this.isSearchVisible = false;
    this.loader = true;
    this.getAdmins();
  }

  isSuperAdmin():boolean{
    return this.currentUserService.isSuperAdmin()
  }

  getCurrentUser(){
    return this.currentUserService.getUser()
  }
}
