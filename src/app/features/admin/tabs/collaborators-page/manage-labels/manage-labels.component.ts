import { Simple } from '../../../../../shared/models/ViewModels/Simple';
import { CollaboratorsLabelsService } from '../../../../../core/api/collaborators-labels.service';
import { Component, Input } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-manage-labels',
  templateUrl: './manage-labels.component.html',
  styleUrls: ['./manage-labels.component.scss'],
})
export class ManageLabelsComponent {
  @Input() labelsList!: Array<Simple>;
  newTag!: string;

  constructor(
    private tagService: CollaboratorsLabelsService,
    private message: NzMessageService
  ) {}

  addTag() {
    let tag = { id: 0, name: this.newTag };
    this.tagService.postLabel(tag).subscribe(
      (res) => {
        this.labelsList.push(tag);
        this.newTag='';
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  deleteTag(index:any, id:number) {
    this.tagService.deleteLabel(id).subscribe(
      (res) => {
        this.labelsList.splice(index, 1);
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }
}
