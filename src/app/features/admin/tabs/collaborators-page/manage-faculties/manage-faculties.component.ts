import { FacultyService } from './../../../../../core/api/faculty.service';
import { Component, Input } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Simple } from 'src/app/shared/models/ViewModels/Simple';

@Component({
  selector: 'app-manage-faculties',
  templateUrl: './manage-faculties.component.html',
  styleUrls: ['./manage-faculties.component.scss'],
})
export class ManageFacultiesComponent {
  @Input() facultiesList!: Array<Simple>;
  newFaculty!: string;

  constructor(
    private facultyService: FacultyService,
    private message: NzMessageService
  ) {}

  addFaculty() {
    let faculty = { id: 0, name: this.newFaculty };
    this.facultyService.postFaculty(faculty).subscribe(
      (res) => {
        this.facultiesList.push(faculty);
        this.newFaculty='';
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  deleteFaculty(index:any, id:number) {
    this.facultyService.deleteFaculty(id).subscribe(
      (res) => {
        this.facultiesList.splice(index, 1);
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }
}
