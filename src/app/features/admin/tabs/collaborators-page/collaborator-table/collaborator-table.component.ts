import { Collaborator } from './../../../../../shared/models/ViewModels/Collaborator';
import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CollaboratorsService } from 'src/app/core/api/collaborators.service';
import { Simple } from 'src/app/shared/models/ViewModels/Simple';

@Component({
  selector: 'app-collaborator-table',
  templateUrl: './collaborator-table.component.html',
  styleUrls: ['./collaborator-table.component.scss'],
})
export class CollaboratorsTableComponent implements OnInit, OnChanges {
  @Input() reloadTable!: number;
  @Input() collaboratorLabels!: Array<Simple>;
  @Input() facultiesList!: Array<Simple>;

  @Output() openEditModal: EventEmitter<number> = new EventEmitter<number>();

  totalNo!: number;
  listOfCollaborators: Array<Collaborator> = new Array<Collaborator>();
  openedCollaborator!:Collaborator;

  isPreviewModalOpen:boolean=false;
  isSearchByNameVisible: boolean = false;
  loader: boolean = false;

  filterData: {
    pageSize: number;
    pageNo: number;
    name: string;
  } = {
    pageSize: 10,
    pageNo: 1,
    name: '',
  };

  constructor(
    private collaboratorsService: CollaboratorsService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.getCollaborators();
  }

  ngOnChanges() {
    this.getCollaborators();
  }

  getCollaborators() {
    this.loader = true;
    this.collaboratorsService
      .getCollaboratorsWithFilters(this.filterData)
      .subscribe(
        (res) => {
          this.totalNo = res.totalNo;
          this.listOfCollaborators = res.collaborators;
          this.loader = false;
        },
        (error) => {
          console.error(error);
          this.message.error('Error! Please try again later.', {
            nzDuration: 5000,
          });
        }
      );
  }

  deleteCollaboratorById(id: number) {
    this.loader = true;
    this.collaboratorsService.deleteCollaborator(id).subscribe(
      (res) => {
        this.getCollaborators();
        this.message.success('The collaborator was deleted successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  editCollaborator(id: number) {
    this.openEditModal.emit(id);
  }

  resetSearch(type: string): void {
    this.filterData.name = '';
    this.searchByName();
  }

  searchByName(): void {
    this.isSearchByNameVisible = false;
    this.loader = true;
    this.getCollaborators();
  }

  getLabelName(id: number): string {
    const result = this.collaboratorLabels?.filter((obj) => obj.id == id);
    return result[0]?.name;
  }

  getFacultyName(id: number): string {
    const result = this.facultiesList?.filter((obj) => obj.id == id);
    return result[0]?.name;
  }

  viewArticles(collaborator: Collaborator) {
    this.openedCollaborator=collaborator;
    this.isPreviewModalOpen=true;
  }

  handleClosePreviewModal(){
    this.isPreviewModalOpen=false;
  }
}
