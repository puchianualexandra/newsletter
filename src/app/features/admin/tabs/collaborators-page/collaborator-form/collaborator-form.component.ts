import { Simple } from './../../../../../shared/models/ViewModels/Simple';
import { Component, Input, OnInit } from '@angular/core';
import { Collaborator } from 'src/app/shared/models/ViewModels/Collaborator';

@Component({
  selector: 'app-collaborator-form',
  templateUrl: './collaborator-form.component.html',
  styleUrls: ['./collaborator-form.component.scss']
})
export class CollaboratorsFormComponent implements OnInit {
  @Input() collaborator!:Collaborator
  @Input() collaboratorLabels: Array<Simple>= new Array<Simple>();
  @Input() listOfFaculties:Array<Simple>=new Array<Simple>();

  constructor() { }

  ngOnInit(): void {
  }

}
