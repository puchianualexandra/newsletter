import { Simple } from './../../../../../shared/models/ViewModels/Simple';
import { FacultyService } from './../../../../../core/api/faculty.service';
import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { CollaboratorsLabelsService } from 'src/app/core/api/collaborators-labels.service';
import { CollaboratorsService } from 'src/app/core/api/collaborators.service';
import { Collaborator } from 'src/app/shared/models/ViewModels/Collaborator';
import { CustomValidators } from 'src/app/shared/utils/custom-validators';

@Component({
  selector: 'app-collaborators-page',
  templateUrl: './collaborators-page.component.html',
  styleUrls: ['./collaborators-page.component.scss'],
})
export class CollaboratorsPageComponent implements OnInit {
  isCollaboratorsModalVisible: boolean = false;
  isLabelsModalVisible: boolean = false;
  isFacultiesModalVisible: boolean = false;

  collaborator!: Collaborator;
  reloadTable: number = 0;

  collaboratorLabels: Array<Simple> = new Array<Simple>();
  facultiesList: Array<Simple> = new Array<Simple>();

  constructor(
    private collaboratorsService: CollaboratorsService,
    private collaboratorsLabelsService: CollaboratorsLabelsService,
    private facultyService: FacultyService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.getCollaboratorLabels();
    this.getFacultiesList();
  }

  getCollaboratorLabels() {
    this.collaboratorsLabelsService.getLabels().subscribe((res) => {
      this.collaboratorLabels = res;
    });
  }

  getFacultiesList() {
    this.facultyService.getFaculties().subscribe((res) => {
      this.facultiesList = res;
    });
  }

  saveCollaborator() {
    if (this.collaborator.id == 0) {
      this.addCollaborator();
    } else {
      this.updateCollaborator();
    }
  }

  addCollaborator() {
    this.collaboratorsService.postCollaborator(this.collaborator).subscribe(
      (res) => {
        if(res.response=='Email already exist!'){
          this.message.error('Error! Email already exist!', {
            nzDuration: 5000,
          });
        }else{
          this.reloadTable++;
          this.isCollaboratorsModalVisible = false;
          this.message.success('The collaborator was added successfully.', {
            nzDuration: 5000,
          });
        }
       
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  updateCollaborator() {
    this.collaboratorsService.updateCollaborator(this.collaborator).subscribe(
      (res) => {
        this.reloadTable++;
        this.isCollaboratorsModalVisible = false;
        this.message.success('The collaborator was updated successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }
  

  getCollaboratorById(id:number){
    this.collaboratorsService.getCollaboratorById(id).subscribe(
      (res) => {
        this.collaborator = res;
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  openAddNewCollaboratorModal() {
    this.isCollaboratorsModalVisible = true;
    this.collaborator={id:0, firstName:'', lastName:'',email:'', phoneNumber:'',facultyId:0, photoUrl:'',function:'', description:'',labelsId:[]}
  }

  openEditCollaboratorModal(id: number) {
    this.isCollaboratorsModalVisible = true;
    this.getCollaboratorById(id);
  }

  handleCancel() {
    this.isCollaboratorsModalVisible = false;
    this.isLabelsModalVisible = false;
    this.isFacultiesModalVisible = false;
    this.getFacultiesList();
    this.getCollaboratorLabels();
  }

  isFormValid() {
    if(this.collaborator){
      return !(
        this.collaborator.firstName == '' ||
        this.collaborator.lastName == '' ||
        this.collaborator.email == '' ||
        this.collaborator.phoneNumber == ''||
        this.collaborator.function =='' ||
        this.collaborator.facultyId ==null ||
        this.collaborator.labelsId.length==0 ||
        this.collaborator.description==''||
        !CustomValidators.validateEmail(this.collaborator.email)
      );
    }
    return
  }
}
