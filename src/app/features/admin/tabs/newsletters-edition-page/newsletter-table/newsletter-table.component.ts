import { Component, EventEmitter, Input, OnInit, Output, OnChanges} from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NewsletterEditionsService } from 'src/app/core/api/newsletter-editions.service';
import { Newsletter } from 'src/app/shared/models/ViewModels/newsletter';

@Component({
  selector: 'app-newsletter-table',
  templateUrl: './newsletter-table.component.html',
  styleUrls: ['./newsletter-table.component.scss'],
})
export class NewsletterTableComponent implements OnInit, OnChanges {
  @Input() reloadTable!: number;
  @Output() openEditModal: EventEmitter<number> = new EventEmitter<number>();

  listOfNewsletter: Array<Newsletter> = new Array<Newsletter>();
  loader: boolean = false;
  totalNo!: number;
  filterData: {
    pageSize: number;
    pageNo: number;
  } = {
    pageSize: 10,
    pageNo: 1,
  };

  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  constructor(
    private newsletterService: NewsletterEditionsService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.getNewsletters();
  }

  ngOnChanges() {
    this.getNewsletters();
  }

  getNewsletters() {
    this.loader = true;
    this.newsletterService
      .getNewslettersWithPagination({
        pageNo: this.filterData.pageNo,
        pageSize: this.filterData.pageSize,
      })
      .subscribe(
        (res) => {
          this.listOfNewsletter = res.newsletterEditions;
          this.totalNo = res.totalNo;
          this.loader = false;
        },
        (error) => {
          console.error(error);
          this.message.error('Error! Please try again later.', {
            nzDuration: 5000,
          });
        }
      );
  }

  deleteNewsletterById(id: number) {
    this.loader = true;
    this.newsletterService.deleteNewsletter(id).subscribe(
      (res) => {
        this.getNewsletters();
        this.message.success('The newsletter has been deleted successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  changeIsCurrent(id: number) {
    this.loader = true;
    this.newsletterService.changeCurrentEdition(id).subscribe(
      (res) => {
        this.getNewsletters();
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  editNewsletter(id: number) {
    this.openEditModal.emit(id);
  }

  openNewsletter(url: string) {
    window.open(url, '_blank');
  }
}
