import { Component } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NewsletterEditionsService } from 'src/app/core/api/newsletter-editions.service';
import { Newsletter } from 'src/app/shared/models/ViewModels/newsletter';

@Component({
  selector: 'app-newsletters-page',
  templateUrl: './newsletters-page.component.html',
  styleUrls: ['./newsletters-page.component.scss'],
})
export class NewslettersPageComponent {
  isModalVisible: boolean = false;
  newsletter!: Newsletter;
  reloadTable: number = 0;

  constructor(
    private newsletterService: NewsletterEditionsService,
    private message: NzMessageService
  ) {}

  openAddNewNewsletterModal() {
    this.isModalVisible = true;
    this.newsletter={id:0, editionDate:new Date(), dateOfUpload:new Date(), url:'',description:'',isCurrent:true}
  }

  openEditArticleModal(id: number) {
    this.isModalVisible = true;
    this.getNewslettersById(id);
  }

  getNewslettersById(id: number) {
    this.newsletterService.getNewslettersById(id).subscribe(
      (res) => {
        this.newsletter = res;
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  saveNewsletter() {
    if (this.newsletter.id == 0) {
      this.postNewsletter();
    } else {
      this.updateNewsletter();
    }
  }

  postNewsletter() {
    this.newsletterService.postNewsletter(this.newsletter).subscribe(
      (res) => {
        this.reloadTable++;
        this.isModalVisible = false;
        this.message.success('The newsletter has been added successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  updateNewsletter() {
    this.newsletterService.updateNewsletter(this.newsletter).subscribe(
      (res) => {
        this.reloadTable++;
        this.isModalVisible = false;
        this.message.success('The newsletter has been updated successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }

  handleCancel() {
    this.isModalVisible = false;
  }

  isFormValid() {
    return !(
      this.newsletter.editionDate == null ||
      this.newsletter.dateOfUpload == null ||
      this.newsletter.url == '' ||
      this.newsletter.description == '' 
    );
  }
}
