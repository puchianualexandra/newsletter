import { SuggestionService } from 'src/app/core/api/suggestion.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  selectedIndex!: number;
  noOfNewSuggestions!:number;

  constructor(private suggestionsService: SuggestionService) {}

  ngOnInit(): void {
  }

  selectTab(tabNo: number){
    this.selectedIndex=tabNo;
    this.getSuggestionsNumber();
    if(this.selectedIndex==6){
      this.readSuggestions()
    }
  }

  getSuggestionsNumber(){
    this.suggestionsService.getNewSuggestions().subscribe(
      (res)=>{
        this.noOfNewSuggestions=res;
      },(error)=>{
        console.error(error)
      }
    )
  }

  readSuggestions(){
    this.suggestionsService.changeNewSuggestion().subscribe(
      (res)=>{
        this.noOfNewSuggestions=0
      },
      (error)=>{
        console.error(error)
      }
    )
  }

}
