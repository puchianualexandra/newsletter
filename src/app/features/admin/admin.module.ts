import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { SubscribersPageComponent } from './tabs/subscribers-page/subscribers-page.component';
import { ArticlesPageComponent } from './tabs/articles-page/articles-page/articles-page.component';
import { ArticleFormComponent } from './tabs/articles-page/article-form/article-form.component';
import { ArticleTableComponent } from './tabs/articles-page/article-table/article-table.component';
import { NewslettersPageComponent } from './tabs/newsletters-edition-page/newsletters-page/newsletters-page.component';
import { NewsletterFormComponent } from './tabs/newsletters-edition-page/newsletter-form/newsletter-form.component';
import { NewsletterTableComponent } from './tabs/newsletters-edition-page/newsletter-table/newsletter-table.component';
import { CollaboratorsPageComponent } from './tabs/collaborators-page/collaborators-page/collaborators-page.component';
import { CollaboratorsTableComponent } from './tabs/collaborators-page/collaborator-table/collaborator-table.component';
import { CollaboratorsFormComponent } from './tabs/collaborators-page/collaborator-form/collaborator-form.component';
import { ManageLabelsComponent } from './tabs/collaborators-page/manage-labels/manage-labels.component';
import { ManageFacultiesComponent } from './tabs/collaborators-page/manage-faculties/manage-faculties.component';
import { SuggestionsPageComponent } from './tabs/suggestions-page/suggestions-page.component';
import { AdminsPageComponent } from './tabs/admins-page/admins-page/admins-page.component';
import { AdminFormComponent } from './tabs/admins-page/admin-form/admin-form.component';
import { AdminTableComponent } from './tabs/admins-page/admin-table/admin-table.component';

@NgModule({
  declarations: [
    AdminDashboardComponent,
    SubscribersPageComponent,
    ArticlesPageComponent,
    ArticleFormComponent,
    ArticleTableComponent,
    NewslettersPageComponent,
    NewsletterFormComponent,
    NewsletterTableComponent,
    CollaboratorsPageComponent,
    CollaboratorsTableComponent,
    CollaboratorsFormComponent,
    ManageLabelsComponent,
    ManageFacultiesComponent,
    SuggestionsPageComponent,
    AdminsPageComponent,
    AdminFormComponent,
    AdminTableComponent,
  ],
  imports: [CommonModule, AdminRoutingModule, SharedModule],
})
export class AdminModule {}
