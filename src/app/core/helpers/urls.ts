export enum Urls {
  YOUTUBE = 'https://www.youtube.com/channel/UC5PbsYmTh2kJuK9G9-GJfZQ',
  FACEBOOK = 'https://www.facebook.com/unitbv/',
  INSTAGRAM = 'https://www.instagram.com/universitateatransilvania/',

  BASE = '',

  HOMEPAGE = 'news/homepage',
  CURRENT_NUMBER = 'news/current-number',
  ARCHIVE='news/archive',
  ALUMNI = 'news/special-sections/alumni',
  GRADUATES='news/special-sections/graduates',
  TEACHERS = 'news/special-sections/teachers',
  OTHERS='news/special-sections/others',
  COLLABORATORS='news/contributors',
  CONTACT='news/contact',

  ADMIN = 'admin',
  LOGIN = 'admin/login',
  ADMIN_DASHBOARD='admin/dashboard',
  ADMIN_ADMINS_PAGE = 'admin/dashboard/users-page',
  ADMIN_SUBSCRIBERS_PAGE = 'admin/dashboard/subscribers-page',
  ADMIN_COLLABORATORS_PAGE = 'admin/dashboard/collaborators-page',
  ADMIN_NEWSLETTERS_PAGE = 'admin/dashboard/newsletters-page',
  ADMIN_ARTICLES_PAGE = 'admin/dashboard/articles-page',
  ADMIN_SUGGESTIONS_PAGE = 'admin/dashboard/suggestions-page',
}
