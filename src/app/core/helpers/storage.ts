export class StorageHelper {
  private static readonly jwtKey: string = 'jwtToken';
  private static readonly currentUserId: string = 'currentUserId';

  constructor() {}

  public static saveJWTInLocalStorage(value: string): void {
    localStorage.setItem(this.jwtKey, value);
  }

  public static getJWT(): any {
    if (localStorage.getItem(this.jwtKey)) {
      return localStorage.getItem(this.jwtKey);
    }
  }

  public static savecurrentUserIdInLocalStorage(value: string): void {
    localStorage.setItem(this.currentUserId, value);
  }

  public static getcurrentUserId(): any {
    if (localStorage.getItem(this.currentUserId)) {
      return localStorage.getItem(this.currentUserId);
    }
  }

  public static clearTokens(): void {
    localStorage.clear();
  }
}
