import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private readonly resourceUrl: string = '/admins';

  constructor(private apiService: ApiService) { }

  getAdmins(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}`)
  }

  getAdminsWithFilters(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}/withFilters`, body)
  }

  getAdminById(id:number): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/${id}`)
  }

  registerAdmin(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}/register`, body)
  }

  deleteAdmin(id:number): Observable<any>{
    return this.apiService.delete(`${this.resourceUrl}/${id}`)
  }

  updateAdmin(id:number, body:any): Observable<any>{
    return this.apiService.put(`${this.resourceUrl}/${id}`, body)
  }

  changePassword(id:number, password:string): Observable<any>{
    return this.apiService.put(`${this.resourceUrl}/changePassword/${id}?password=${password}`)
  }

}
