import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class CollaboratorsLabelsService {

  private readonly resourceUrl: string = '/labels';

  constructor(private apiService: ApiService) { }

  getLabels(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}`)
  }

  getLabelById(id:number): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/${id}`)
  }

  postLabel(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}`, body)
  }

  deleteLabel(id:number): Observable<any>{
    return this.apiService.delete(`${this.resourceUrl}/${id}`)
  }
}
