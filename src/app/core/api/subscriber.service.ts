import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class SubscriberService {
  private readonly resourceUrl: string = '/subscribers';

  constructor(private apiService: ApiService) { }

  getSubscribersWithFilters(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}/withFilters`, body)
  }

  getSubscriberByEmail(email:string): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/byEmail?email=${email}`)
  }

  postSubscriber(email:string): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}?email=${email}`)
  }

  deleteSubscriber(id:number): Observable<any>{
    return this.apiService.delete(`${this.resourceUrl}/${id}`)
  }
}