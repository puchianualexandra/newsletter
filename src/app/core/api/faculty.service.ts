import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class FacultyService {

  private readonly resourceUrl: string = '/faculties';

  constructor(private apiService: ApiService) { }

  getFaculties(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}`)
  }

  getFacultyById(id:number): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/${id}`)
  }

  postFaculty(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}`, body)
  }

  deleteFaculty(id:number): Observable<any>{
    return this.apiService.delete(`${this.resourceUrl}/${id}`)
  }
}
