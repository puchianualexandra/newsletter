import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class CollaboratorsService {

  private readonly resourceUrl: string = '/collaborators';

  constructor(private apiService: ApiService) { }

  getCollaborators(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}`)
  }

  getCollaboratorsWithFilters(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}/withFilters`, body)
  }

  getCount(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/count`)
  }

  updateCollaborator(body:any): Observable<any>{
    return this.apiService.put(`${this.resourceUrl}/${body.id}`, body)
  }

  getCollaboratorById(id:number | null): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/${id}`)
  }

  postCollaborator(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}`, body)
  }

  deleteCollaborator(id:number): Observable<any>{
    return this.apiService.delete(`${this.resourceUrl}/${id}`)
  }
}
