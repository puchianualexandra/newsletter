import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Newsletter } from 'src/app/shared/models/ViewModels/newsletter';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class NewsletterEditionsService {

  private readonly resourceUrl: string = '/newsletterEditions';

  constructor(private apiService: ApiService) { }

  getNewslettersByYear(year:number): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/${year}`)
  }

  getNewslettersById(id:number): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/byId/${id}`)
  }

  getNewslettersWithPagination(body:{pageNo:number, pageSize:number}): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}/withPagination`, body)
  }

  getCurrentEdition(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/currentEdition`)
  }

  getCount(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/count`)
  }

  postNewsletter(body: Newsletter): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}`, body)
  }

  updateNewsletter(body: Newsletter): Observable<any>{
    return this.apiService.put(`${this.resourceUrl}/${body.id}`, body)
  }

  deleteNewsletter(id:number): Observable<any>{
    return this.apiService.delete(`${this.resourceUrl}/${id}`)
  }

  changeCurrentEdition(id:number): Observable<any>{
    return this.apiService.put(`${this.resourceUrl}/changeCurrentEdition/${id}`)
  }
}
