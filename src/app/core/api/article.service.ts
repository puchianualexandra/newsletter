import { Article } from 'src/app/shared/models/ViewModels/article';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private readonly resourceUrl: string = '/articles';

  constructor(private apiService: ApiService) { }

  getArticle(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}`)
  }

  getArticleById(id:number): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/${id}`)
  }

  postArticle(body:Article): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}`, body)
  }

  updateArticle(body:Article): Observable<any>{
    return this.apiService.put(`${this.resourceUrl}/${body.id}`, body)
  }

  deleteArticle(id:number): Observable<any>{
    return this.apiService.delete(`${this.resourceUrl}/${id}`)
  }

  getCount(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/count`)
  }

  getArticlesByCollaborator(id:number): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/getByCollaboratorId/${id}`)
  }

  getArticleItems(id:number): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/getArticleItems/${id}`)
  }

  getArticlesWithFiltersOnUserSide(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}/withFiltersUser`, body)
  }

  getArticlesWithFiltersOnAdminSide(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}/withFiltersAdmin`, body)
  }

  changeIsPosted(id:number): Observable<any>{
    return this.apiService.put(`${this.resourceUrl}/changeIsPosted/${id}`)
  }
}
