import { Suggestion } from './../../shared/models/ViewModels/Suggestion';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class SuggestionService {

  private readonly resourceUrl: string = '/suggestions';

  constructor(private apiService: ApiService) { }

  getSuggestionsWithFilters(body:any): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}/withFilters`, body)
  }

  getNewSuggestions(): Observable<any>{
    return this.apiService.get(`${this.resourceUrl}/newSuggestionsNo`)
  }

  postSuggestion(body:Suggestion): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}`, body)
  }

  deleteSuggestion(id:number): Observable<any>{
    return this.apiService.delete(`${this.resourceUrl}/${id}`)
  }

  changeNewSuggestion(): Observable<any>{
    return this.apiService.put(`${this.resourceUrl}/changeNewSuggestions`)
  }
}
