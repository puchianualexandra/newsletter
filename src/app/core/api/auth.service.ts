import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly resourceUrl: string = '/admins';

  constructor(private apiService: ApiService) { }

  logIn(body:{email:string, password:string}): Observable<any>{
    return this.apiService.post(`${this.resourceUrl}/login`, body)
  }
}
