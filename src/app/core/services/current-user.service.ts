import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Admin } from 'src/app/shared/models/ViewModels/Admin';

@Injectable({
  providedIn: 'root',
})
export class CurrentUserService {
  private user!: Admin;

  private userSource: Subject<Admin> = new Subject<Admin>();
  user$ = this.userSource.asObservable();

  constructor() {}

  getUser(): Admin {
    return this.user;
  }

  setUser(user: Admin): Admin {
    this.user = user;
    this.userSource.next(user);
    return this.user;
  }

  isSuperAdmin(): boolean {
    if (this.user) {
      if (this.user.role == 'Super Admin') {
        return true;
      }
    }
    return false;
  }
}
