export interface ArticleItem {
  id:number;
  type: string;
  contentImage: string;
  contentText: string;
  acticleId?:number;
}
