import { ArticleItem } from './articleItem';
import { Collaborator } from './Collaborator';

export interface Article {
  id: number;
  title: string;
  dateOfUpload: Date;
  collaboratorId: number | null;
  collaborator?: Collaborator;
  description: string;
  specialSection:string,
  articleItemsDTO: Array<ArticleItem>;
  isPosted:boolean;
}
