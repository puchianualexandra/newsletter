export interface Newsletter {
  id: number;
  editionDate: Date;
  url: string;
  dateOfUpload: Date;
  description: string;
  isCurrent: boolean;
}
