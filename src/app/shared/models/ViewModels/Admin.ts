export interface Admin {
  id: number;
  password?:string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  dateOfJoin: Date;
  isNew: boolean;
  role: string;
}
