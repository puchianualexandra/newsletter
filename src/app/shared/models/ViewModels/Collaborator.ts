export interface Collaborator {
    id:number;
    firstName: string;
    lastName:string;
    email:string;
    phoneNumber:string;
    facultyId:number;
    function:string;
    photoUrl:string,
    description: string;
    labelsId:Array<number>
    articles?:Array<any>;
  }