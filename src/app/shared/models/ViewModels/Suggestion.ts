export interface Suggestion {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  subject: string;
  description: string;
  dateOfMessage: Date;
  isNew:boolean;
}
