import { Admin } from './../ViewModels/Admin';

export interface loginDTO {
   isSuccessful:boolean;
   message:string;
   token:string;
   user:Admin;
  }