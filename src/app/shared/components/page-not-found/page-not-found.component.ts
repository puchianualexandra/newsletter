import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Urls } from 'src/app/core/helpers/urls';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent {

  constructor(private router: Router) { }

  redirectToHomepage(){
    this.router.navigate([Urls.HOMEPAGE]);
  }

}
