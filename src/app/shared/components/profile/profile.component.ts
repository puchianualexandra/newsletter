import { AdminService } from './../../../core/api/admin.service';
import { Admin } from 'src/app/shared/models/ViewModels/Admin';
import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  @Input() user!: Admin;

  constructor(
    private adminService: AdminService,
    private message: NzMessageService
  ) {}

  ngOnInit(): void {}

  updateProfile() {
    this.adminService.updateAdmin(this.user.id, this.user).subscribe(
      (res) => {
        this.message.success('The profile has been updated successfully.', {
          nzDuration: 5000,
        });
      },
      (error) => {
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    );
  }
}
