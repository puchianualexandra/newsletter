import { Component, Input, OnInit } from '@angular/core';
import { CollaboratorsLabelsService } from 'src/app/core/api/collaborators-labels.service';
import { FacultyService } from 'src/app/core/api/faculty.service';
import { Article } from '../../models/ViewModels/article';
import { Simple } from '../../models/ViewModels/Simple';

@Component({
  selector: 'app-view-article',
  templateUrl: './view-article.component.html',
  styleUrls: ['./view-article.component.scss'],
})
export class ViewArticleComponent implements OnInit {
  @Input() article!: Article;

  labels!: Simple[];
  faculties!: Simple[];

  constructor(
    private labelsService: CollaboratorsLabelsService,
    private facultyService: FacultyService
  ) {}

  ngOnInit(): void {
    this.getLabels();
    this.getFaculties();
  }

  getLabels() {
    this.labelsService.getLabels().subscribe((res) => {
      this.labels = res;
    });
  }

  getFaculties() {
    this.facultyService.getFaculties().subscribe((res) => {
      this.faculties = res;
    });
  }

  getLabelNameById(id: number) {
    if (this.labels) {
      const result = this.labels?.filter((obj) => obj.id == id);
      return result[0]?.name;
    }
    return;
  }

  getFacultyNameById(id: number | undefined) {
    if (this.faculties && id) {
      const result = this.faculties?.filter((obj) => obj.id == id);
      return result[0]?.name;
    }
    return;
  }
}
