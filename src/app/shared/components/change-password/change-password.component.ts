import { AdminService } from './../../../core/api/admin.service';
import { Admin } from './../../models/ViewModels/Admin';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  @Input() user!:Admin;
  @Input() isNew!:boolean;
  @Output() close:EventEmitter<any>=new EventEmitter<any>()

  passwordVisible:boolean = false;
  confirmPasswordVisible:boolean = false;
  password: string='';
  confirmPassword:string='';

  constructor(private adminService:AdminService, private message: NzMessageService,) { }

  ngOnInit(): void {
  }

  changePassword(){
    this.adminService.changePassword(this.user.id, this.password).subscribe(
      (res)=>{
        this.message.success('The password has been changed.', {
          nzDuration: 5000,
        });
        this.close.emit()
      },(error)=>{
        console.error(error);
        this.message.error('Error! Please try again later.', {
          nzDuration: 5000,
        });
      }
    )
  }

  isTheSamePassword():boolean{
    return this.password==this.confirmPassword
  }

}
