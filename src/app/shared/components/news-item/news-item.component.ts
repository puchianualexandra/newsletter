import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Newsletter } from 'src/app/shared/models/ViewModels/newsletter';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.scss'],
})
export class NewsItemComponent implements OnInit, OnChanges {
  @Input() item!: Newsletter;
  itemTitle: string = '';
  months: Array<string> = new Array<string>();

  constructor(private translate: TranslateService) {}

  ngOnInit(): void {
    this.translate.onLangChange.subscribe((event: any) => {
      this.getListOfMonths();
    });
  }

  ngOnChanges() {
    this.getListOfMonths();
  }

  getTitle() {
    let date = new Date(this.item?.editionDate);
    let month = this.months[date.getMonth()];
    let year = date.getFullYear();

    this.itemTitle = month + ' ' + year;
  }

  getListOfMonths() {
    if (this.translate.currentLang == 'en') {
      this.months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];
    } else
      this.months = [
        'Ianuarie',
        'Februarie',
        'Martie',
        'Aprilie',
        'Mai',
        'Iunie',
        'Iulie',
        'August',
        'Septembrie',
        'Octombrie',
        'Noiembrie',
        'Decembrie',
      ];

    this.getTitle();
  }
}
