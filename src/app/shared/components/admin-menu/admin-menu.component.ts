import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Urls } from 'src/app/core/helpers/urls';

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss'],
})
export class AdminMenuComponent implements OnInit {
  @Input() noOfNewSuggestions!:number;
  @Output() selectedTab: EventEmitter<number> = new EventEmitter<number>();
  selectedIndex!: number;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.selectTab(1);
  }

  selectTab(index: number): void {
    this.selectedIndex = index;
    this.selectedTab.emit(this.selectedIndex);

    switch (this.selectedIndex) {
      case 1:
        this.router.navigateByUrl(Urls.ADMIN_ADMINS_PAGE);
        break;
      case 2:
        this.router.navigateByUrl(Urls.ADMIN_SUBSCRIBERS_PAGE);
        break;
      case 3:
        this.router.navigateByUrl(Urls.ADMIN_COLLABORATORS_PAGE);
        break;
      case 4:
        this.router.navigateByUrl(Urls.ADMIN_NEWSLETTERS_PAGE);
        break;
      case 5:
        this.router.navigateByUrl(Urls.ADMIN_ARTICLES_PAGE);
        break;
      case 6:
          this.router.navigateByUrl(Urls.ADMIN_SUGGESTIONS_PAGE);
          break;
      default:
        break;
    }
  }
}
