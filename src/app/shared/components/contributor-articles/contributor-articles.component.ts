import { Article } from 'src/app/shared/models/ViewModels/article';
import { Router } from '@angular/router';
import { ArticleService } from 'src/app/core/api/article.service';
import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { Collaborator } from 'src/app/shared/models/ViewModels/Collaborator';
import { Urls } from 'src/app/core/helpers/urls';

@Component({
  selector: 'app-contributor-articles',
  templateUrl: './contributor-articles.component.html',
  styleUrls: ['./contributor-articles.component.scss'],
})
export class ContributorArticlesComponent implements OnInit, OnChanges {
  @Input() collaborator!: Collaborator;
  openDescription: Array<boolean> = new Array<boolean>();

  constructor(
    private articlesService: ArticleService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    this.getCollaboratorArticles();
  }

  getCollaboratorArticles() {
    if (this.collaborator) {
      this.articlesService
        .getArticlesByCollaborator(this.collaborator.id)
        .subscribe(
          (res) => {
            this.collaborator.articles = res;
          },
          (error) => {
            console.error(error);
          },
          () => {
            this.collaborator.articles?.forEach((element) => {
              this.openDescription.push(false);
            });
          }
        );
    }
  }

  isOnAdmin() {
    return this.router.url.includes('/admin/dashboard');
  }

  openArticle(article: Article) {
    if (article.specialSection == 'alumni') {
      this.router.navigate([Urls.ALUMNI], {
        queryParams: { titleArticle: article.title },
      });
      return;
    }

    if (article.specialSection == 'graduates') {
      this.router.navigate([Urls.GRADUATES], {
        queryParams: { titleArticle: article.title },
      });
      return;
    }

    if (article.specialSection == 'teachers') {
      this.router.navigate([Urls.TEACHERS], {
        queryParams: { titleArticle: article.title },
      });
      return;
    }

    if (article.specialSection == 'others') {
      this.router.navigate([Urls.OTHERS], {
        queryParams: { titleArticle: article.title },
      });
      return;
    }
  }
}
