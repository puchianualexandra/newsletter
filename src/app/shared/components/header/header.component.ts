import { AdminService } from './../../../core/api/admin.service';
import { Admin } from 'src/app/shared/models/ViewModels/Admin';
import { CurrentUserService } from './../../../core/services/current-user.service';
import { StorageHelper } from './../../../core/helpers/storage';
import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Urls } from 'src/app/core/helpers/urls';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Output() onLanguageChange: EventEmitter<string> = new EventEmitter<string>();
  subscriptions: Subscription = new Subscription();
  selectedLanguage = 'ro';
  user!:Admin;

  innerWidth: any;
  isCollapsed: boolean = false;
  isNewAdminModalVisible:boolean=false;
  isViewProfileOpen:boolean=false;

  constructor(
    private router: Router,
    private currentUserService: CurrentUserService,
    private adminService:AdminService
  ) {
    this.subscriptions.add(
      currentUserService.user$.subscribe(
        (res)=>{
          this.user=res;
          if(this.user.isNew==true){
            this.openNewAdminModal()
          }
        }
      )
    )
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;

    if(!this.currentUserService.getUser() && StorageHelper.getcurrentUserId()){
      this.getUserById()
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  onLanguageChangeEvent(): void {
    this.onLanguageChange.emit(this.selectedLanguage);
  }

  getUserById(){
    this.adminService.getAdminById(StorageHelper.getcurrentUserId()).subscribe(
      (res)=>{
        this.user=res;
        this.currentUserService.setUser(this.user)
      },(error)=>{
        console.error(error)
      },()=>{
        if(this.user?.isNew==true){
          this.openNewAdminModal();
        }
      }
    )
  }

  openNewAdminModal() {
    this.isNewAdminModalVisible = true;
  }

  openProfileModal() {
    this.isViewProfileOpen = true;
  }

  closeNewAdminModal(event?:any) {
    this.isNewAdminModalVisible = false;
  }

  logout() {
    StorageHelper.clearTokens();
    this.currentUserService.setUser({} as Admin);
    this.router.navigateByUrl(Urls.LOGIN);
  }

  checkUrl(selectedRoute: string): boolean {
    return this.router.url.includes(selectedRoute);
  }

  isOnViewMode() {
    return !this.router.url.includes('/admin');
  }

  isOnAdmin() {
    return this.router.url.includes('/admin/dashboard');
  }
}
