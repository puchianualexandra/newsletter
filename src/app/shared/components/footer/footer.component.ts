import { CustomValidators } from './../../utils/custom-validators';
import { SubscriberService } from './../../../core/api/subscriber.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  subscribeEmail: string = '';
  innerWidth: any;

  constructor(
    private message: NzMessageService,
    private router: Router,
    private subscriberService: SubscriberService
  ) {}

  ngOnInit(){
    this.innerWidth = window.innerWidth;
  }

  subscribe() {
    if (this.subscribeEmail != '') {
      if (CustomValidators.validateEmail(this.subscribeEmail) == false) {
        this.message.warning('You have to enter a valid email!', {
          nzDuration: 5000,
        });
      } else {
        this.subscriberService.postSubscriber(this.subscribeEmail).subscribe(
          (res) => {
            if (res.response == 'Success!') {
              this.message.success(
                "You' successfully subscribet to our newsletter.",
                {
                  nzDuration: 5000,
                }
              );
            } else {
              this.message.error(res.response, {
                nzDuration: 5000,
              });
            }
          },
          (error) => {
            console.error(error);
            this.message.error(
              'There was a problem to subscribe. Please try again later.',
              {
                nzDuration: 5000,
              }
            );
          }
        );
      }
    } else {
      this.message.warning('You have to enter a valid email!', {
        nzDuration: 5000,
      });
    }
  }

  cleanSearchInput() {
    this.subscribeEmail = '';
  }

  isOnViewMode() {
    return !this.router.url.includes('/admin');
  }
}
