import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//components
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

//others
import { TranslateModule } from '@ngx-translate/core';

//ng-zorro
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTableModule } from 'ng-zorro-antd/table';
import { AdminMenuComponent } from './components/admin-menu/admin-menu.component';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { ViewArticleComponent } from './components/view-article/view-article.component';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NewsItemComponent } from './components/news-item/news-item.component';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { ProfileComponent } from './components/profile/profile.component';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { ContributorArticlesComponent } from './components/contributor-articles/contributor-articles.component';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    AdminMenuComponent,
    ViewArticleComponent,
    PageNotFoundComponent,
    NewsItemComponent,
    ProfileComponent,
    ChangePasswordComponent,
    ContributorArticlesComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,

    //ng-zorro
    NzButtonModule,
    NzMenuModule,
    NzIconModule,
    NzInputModule,
    NzToolTipModule,
    NzRadioModule,
    NzSelectModule,
    NzMessageModule,
    NzGridModule,
    NzFormModule,
    NzUploadModule,
    NzTagModule,
    NzAvatarModule,
    NzTabsModule,
    NzTableModule,
    NzDropDownModule,
    NzModalModule,
    NzPaginationModule,
    NzCardModule,
    NzDatePickerModule,
    NzImageModule,
    NzSwitchModule,
    NzPopconfirmModule,
    NzCollapseModule,
    NzSpinModule,
    NzPopoverModule,
    NzEmptyModule,
    NzResultModule,
    NzAlertModule,
    NzDrawerModule,
    NzBadgeModule
  ],
  exports: [
    //components
    HeaderComponent,
    FooterComponent,
    AdminMenuComponent,
    ViewArticleComponent,
    NewsItemComponent,
    ContributorArticlesComponent,

    //ng-zorro
    NzButtonModule,
    NzMenuModule,
    NzIconModule,
    NzInputModule,
    NzToolTipModule,
    NzRadioModule,
    NzSelectModule,
    NzMessageModule,
    NzGridModule,
    NzFormModule,
    NzUploadModule,
    NzTagModule,
    NzAvatarModule,
    NzTabsModule,
    NzTableModule,
    NzDropDownModule,
    NzModalModule,
    NzPaginationModule,
    NzCardModule,
    NzDatePickerModule,
    NzImageModule,
    NzSwitchModule,
    NzPopconfirmModule,
    NzCollapseModule,
    NzSpinModule,
    NzPopoverModule,
    NzEmptyModule,
    NzResultModule,
    NzAlertModule,
    NzDrawerModule,
    NzBadgeModule,

    //others
    TranslateModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class SharedModule {}
